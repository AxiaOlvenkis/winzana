<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FilesServices
{
    private $directory;

    public function __construct($directory)
    {
        $this->directory = $directory;
    }

    public function uploadFile($file): string
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        // this is needed to safely include the file name as part of the URL
        $safeName = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
        $newName = $safeName.'-'.uniqid().'.'.$file->guessExtension();

        // Move the file to the directory where brochures are stored
        try {
            $file->move($this->directory, $newName);
        } catch (FileException $e) {
        }

        return $newName;
    }
}